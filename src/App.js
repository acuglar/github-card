import { useState } from 'react';
import './App.css';
import Card from './components/Card'

const App = () => {

  const [user, setUser] = useState([]);
  const [active, setActive] = useState(false);
  const [input, setInput] = useState('');

  const handleToggle = () => {
    fetch(`https://api.github.com/users/${input}`)
      .then(response => response.json())
      .then(response => setUser(response))
      .catch(error => console.log('There has been a problem with your fetch operation: ' + error.message))
    console.log(input)
  }
  console.log(input)

  const handleActive = () => {
    setActive(!active)
    handleToggle()
  }

  const handleInput = (event) => {
    setInput(event.target.value)
    console.log(event.target.value)
    console.log(input)
  }

  return (
    <div className="App">

      <input type="text" onChange={handleInput} />
      <button onClick={handleActive}>Toggle</button>

      {active && <Card image={user.avatar_url} name={user.name} login={user.login} followers={user.followers} following={user.following}></Card>}
    </div>
  );
}

export default App;
